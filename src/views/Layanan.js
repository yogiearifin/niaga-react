import React from "react";
import { Link } from "react-router-dom";


const Layanan = () => {
    return (
        <div className="footer">
            <ul>
                <li><h3>Layanan</h3></li>
                <li>Domain</li>
                <li>Shared Hosting</li>
                <li>Cloud Hosting</li>
                <li>Cloud VPS Hosting</li>
                <li>Transfer Hosting</li>
                <li>Web Builder</li>
                <li>Keamanan SSL/HTTPS</li>
                <li>Jasa Pembuatan Website</li>
                <li>Program Afiliasi</li>
                <li>Whois</li>
                <li>Niagahoster Status</li>
            </ul>
        </div>
    )
}

export default Layanan;