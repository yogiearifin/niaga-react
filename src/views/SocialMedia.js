import React from "react";



const SocialMedia = () => {
    return (
        <div className="social">
            <img src={require("../assets/images/facebook.webp")}></img>
            <img src={require("../assets/images/insta.png")}></img>
            <img src={require("../assets/images/linkedin.png")}></img>
            <img src={require("../assets/images/twitter.png")}></img>
            
        </div>
    )
}

export default SocialMedia;