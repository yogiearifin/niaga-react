import React from "react";
import "../assets/style/Content.css"

const Payments = () => {
    return (
        <React.Fragment>
            <div className="payments">
                <div class="payments-detail">
                    <h3>Beragam pilihan cara pembayaran yang mempercepat proses order hosting & domain Anda.</h3>
                    <div class="payments-detail-icon-1">
                        <img src={require("../assets/images/bca.png")}></img>
                        <img src={require("../assets/images/Mandiri.png")}></img>
                        <img src={require("../assets/images/bni.webp")}></img>
                        <img src={require("../assets/images/bri.png")}></img>
                        <img src={require("../assets/images/Maybank.png")}></img>
                        <img src={require("../assets/images/cimb.png")}></img>
                        <img src={require("../assets/images/alto.png")}></img>
                        <img src={require("../assets/images/atmbersama.png")}></img>
                        <img src={require("../assets/images/paypal.png")}></img>
                    </div>
                    <div class="payments-detail-icon-2">
                        <img src={require("../assets/images/indomaret.png")}></img>
                        <img src={require("../assets/images/alfamart.png")}></img>
                        <img src={require("../assets/images/pegadaian.png")}></img>
                        <img src={require("../assets/images/posindo.png")}></img>
                        <img src={require("../assets/images/ovo.png")}></img>
                        <img src={require("../assets/images/gopay.png")}></img>
                        <img src={require("../assets/images/visa.png")}></img>
                        <img src={require("../assets/images/mastercard.png")}></img>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Payments;