import React from "react";
import { Link } from "react-router-dom";


const KenapaPilih = () => {
    return (
        <div className="footer">
            <ul>
                <li><h3>Kenapa Pilih Niagahoster</h3></li>
                <li>Hosting Terbaik</li>
                <li>Datacenter Hosting Terbaik</li>
                <li>Domain Gratis</li>
                <li>Bagi-Bagi Domain Gratis</li>
                <li>Bagi-Bagi Hosting Gratis</li>
                <li>Review Pelanggan</li>
            </ul>
        </div>
    )
}

export default KenapaPilih;