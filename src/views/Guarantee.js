import React from "react";
import "../assets/style/Content.css"

const Guarantee = () => {
    return (
        <div className="guarantee">
            <div class="guarantee-detail">
                <div class="guarantee-detail-card">
                    <img src={require("../assets/images/icons-guarantee.svg")}></img>
                    <div class="guarantee-detail-card-text">
                        <h1>Garansi 30 Hari Uang Kembali</h1>
                        <p>Tidak puas dengan layanan hosting Niagahoster? Kami menyediakan<br/>garansi uang kembali yang berlaku 30 hari sejak tanggal pembelian.</p>
                        <button>MULAI SEKARANG</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Guarantee;