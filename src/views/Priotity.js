import React from "react";
import "../assets/style/Content.css"


const Priority = () => {
        return (
        <React.Fragment>
            <div class="feature-priority">
                <div class="feature-priority-card">
                    <h1>Prioritas Kecepatan dan Keamanan</h1>
                    <h2>Hosting Super Cepat</h2>
                    <p>Pengunjung tidak suka website lambat. Dengan dukungan<br/>LiteSpeed Web Server, waktu loading website Anda akan<br/>meningkat pesat.</p>
                    <h2>Keamanan Website Ekstra</h2>
                    <p>Teknologi keamanan Imunify 360 memungkinkan website Anda<br/>terlindung dari serangan hacker, malware, dan virus berbahaya<br/>setiap saat.</p>
                    <button>LIHAT SELENGKAPNYA</button>
                </div>
                <img className="feature-priority-server" src={require("../assets/images/server.webp")}></img>
                <img className="feature-priority-immunify" src={require("../assets/images/imunify.svg")}></img>
                <img className="feature-priority-chart" src={require("../assets/images/graphic.svg")}></img>
                <img className="feature-priority-speed" src={require("../assets/images/lite-speed.svg")}></img>
            </div>
            <div class="feature-pricing">
                <h1>Biaya Hemat, Kualitas Hebat</h1>
                <div class="feature-pricing-card">
                    <h2>Harga Murah, Fitur Lengkap</h2>
                    <p>Anda bisa berhemat dan tetap mendapatkan hosting terbaik<br/>dengan fitur lengkap, dari auto install WordPress, cPanel<br/>lengkap, hingga SSL gratis</p>
                    <h2>Website Selalu Online</h2>
                    <p>Jaminan server uptime 99,98% memungkinkan website Anda<br/>selalu online sehingga Anda tidak perlu khawatir kehilangan<br/>trafik dan pendapatan.</p>
                    <h2>Tim Support Andal dan Cepat Tanggap</h2>
                    <p>Tidak perlu menunggu lama, selesaikan masalah Anda dengan<br/>cepat secara real time melalui live chat 24/7</p>
                </div>
                <img className="feature-pricing-big" src={require("../assets/images/titis.webp")}></img>
                <img className="feature-pricing-intercom" src={require("../assets/images/intercom-logo.svg")}></img>
                <img className="feature-pricing-woman1" src={require("../assets/images/woman1.webp")}></img>
                <img className="feature-pricing-woman2" src={require("../assets/images/woman2.png")}></img>
                <img className="feature-pricing-man" src={require("../assets/images/man.png")}></img>
                <img className="feature-pricing-online" src={require("../assets/images/icon-online.svg")}></img>
            </div>
        </React.Fragment>
    )
}

export default Priority;