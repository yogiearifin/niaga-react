import React from "react";
import "../assets/style/Content.css"

const Packages = () => {
    return (
        <div class="packages-detail">
                <h1 class="packages-detail-header">Pilih Paket Hosting Anda</h1>
                <div class="packages-detail-card">
                    <div class="packages-detail-card-bayi">
                        <div class="termurah-bayi">Termurah!</div>
                        <h1>Bayi</h1>
                        <h2>Rp 10.000</h2><span class="bln-bayi">/bln</span>
                        <button>PILIH SEKARANG</button>
                        <p>Sesuai untuk Pemula atau Belajar<br/>Website</p>
                        <ul>
                            <li>500 MB Disk Space</li>
                            <li>Unlimited Bandwidth</li>
                            <li>Unlimited Database</li>
                            <li>1 Domain</li>
                            <li>Instant Backup</li>
                            <li>Unlimited SSL Gratis Selamanya</li>
                        </ul>
                        <h3>Lihat detail fitur</h3>
                    </div>
                <div class="packages-detail-card-pelajar">
                    <div class="diskon-pelajar">Diskon up to 34 %</div>
                    <h1>Pelajar</h1>
                    <h3 class="pelajar-striketru">Rp 60.800,-</h3>
                    <h2>Rp 40.223</h2><span class="bln-pelajar">/bln</span>
                    <button>PILIH SEKARANG</button>
                    <p>Sesuai untuk Budget Minimal, Landing<br/>Page, Blog Pribadi</p>
                    <ul>
                        <li>Unlimited Disk Space</li>
                        <li>Unlimited Bandwidth</li>
                        <li>Unlimited POP3 Email</li>
                        <li>Unlimited Database</li>
                        <li>10 Addon Domain</li>
                        <li>Instant Backup</li>
                        <li>Domain Gratis</li>
                        <li>Unlimited SSL Gratis Selamanya</li>
                    </ul>
                    <h3>Lihat detail fitur</h3>
                </div>
                <div class="packages-detail-card-personal">
                    <div class="best-seller">
                        Best Seller
                    </div>
                    <div class="arrow-down"></div>
                    <div class="diskon-personal">Diskon up to 75 %</div>
                    <h1>Personal</h1>
                    <h3 class="personal-striketru">Rp 106.250,-</h3>
                    <h2>Rp 26.563</h2><span class="bln-personal">/bln</span>
                    <button>PILIH SEKARANG</button>
                    <p>Sesuai untuk Website Bisnis, UKM,<br/>Organisasi, Komunitas, Toko Online, dll</p>
                    <ul>
                        <li>Unlimited Disk Space</li>
                        <li>Unlimited Bandwidth</li>
                        <li>Unlimited POP3 Email</li>
                        <li>Unlimited Database</li>
                        <li>Unlimited Addon Domain</li>
                        <li>Instant Backup</li>
                        <li>Domain Gratis</li>
                        <li>Unlimited SSL Gratis Selamanya</li>
                        <li>SpamAssassin Mail Protection</li>
                    </ul>
                    <h3>Lihat detail fitur</h3>
                </div>
                <div class="packages-detail-card-bisnis">
                    <div class="diskon-bisnis">Diskon up to 42 %</div>
                    <h1>Bisnis</h1>
                    <h3 class="bisnis-striketru">Rp 147.800,-</h3>
                    <h2>Rp 85.724</h2><span class="bln-bisnis">/bln</span>
                    <button>PILIH SEKARANG</button>
                    <p>Sesuai untuk Website Bisnis, Portal<br/>Berita, Toko Online, dll</p>
                    <ul>
                        <li>Unlimited Disk Space</li>
                        <li>Unlimited Bandwidth</li>
                        <li>Unlimited POP3 Email</li>
                        <li>Unlimited Database</li>
                        <li>Unlimited Addon Domain</li>
                        <li>Magic Auto Backup & Restore</li>
                        <li>Domain Gratis</li>
                        <li>Unlimited SSL Gratis Selamanya</li>
                        <li>Prioritas Layanan Support</li>
                        <li>SpamAssassin Mail Protection</li>
                    </ul>
                    <h3>Lihat detail fitur</h3>
                </div>
                </div>
            </div>
    )
}

export default Packages;