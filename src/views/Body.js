import React from "react";
import "../assets/style/Content.css"
import Intro from "./Intro"
import Service from "./Service"
import Priority from "./Priotity"
import Testimonies from "./Testimonies"
import Packages from "./Packages"
import Clients from "./Clients"
import Faqs from "./Faqs"
import Guarantee from "./Guarantee"
import Payments from "./Payments"
import OrderHosting from "./OrderHosting"


const Body = () => {
    return (
        <div className="Body">
            <Intro/>
            <Service/>
            <Priority/>
            <Testimonies/>
            <Packages/>
            <Clients/>
            <Faqs/>
            <Guarantee/>
            <Payments/>
            <OrderHosting/>
        </div>
    )
}

export default Body;