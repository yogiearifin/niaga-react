import React from "react";
import "../assets/style/Content.css"

const Clients = () => {
    return (
        <React.Fragment>
            <div className="faqs">
                <h1>Dipercaya 52.000+ Pelanggan di Seluruh Indonesia</h1>
                <div class="clients">
                    <img src={require("../assets/images/richese.png")}></img>
                    <img src={require("../assets/images/rabbani.png")}></img>
                    <img src={require("../assets/images/otten.png")}></img>
                    <img src={require("../assets/images/Hydro.png")}></img>
                    <img src={require("../assets/images/petrokimia.png")}></img>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Clients;