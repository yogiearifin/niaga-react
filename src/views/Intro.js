import React from "react";
import "../assets/style/Content.css";


const Intro = () => {
        return (
        <React.Fragment>
        <div className="intro">
            <div className="intro-text">
                <h1>Unlimited Web Hosting<br/> Terbaik di Indonesia</h1>
                <p>Ada banyak peluang bisa Anda raih dari rumah dengan memiliki <br/>website. Manfaatkan diskon hosting hingga 75% dan tetap <br/>produktif di bulan Ramadhan bersama Niagahoster.</p>
                <p>Yuk segera order karena diskon dapat berakhir sewaktu-waktu!</p>
                <button>PILIH SEKARANG</button>
                <img src={require("../assets/images/hero-home-ramadhan.webp")}></img>
            </div>
        </div>
        </React.Fragment>
    )
}

export default Intro;