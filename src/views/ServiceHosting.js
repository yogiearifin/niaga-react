import React from "react";


const ServiceHosting = () => {
    return (
        <div className="footer">
            <ul>
                <li><h3>Service Hosting</h3></li>
                <li>Hosting Murah</li>
                <li>Hosting Indonesia</li>
                <li>Hosting Singapore SG</li>
                <li>Hosting Wordpress</li>
                <li>Email Hosting</li>
                <li>Reseller Hosting</li>
                <li>Web Hosting Unlimited</li>
            </ul>
        </div>
    )
}

export default ServiceHosting;