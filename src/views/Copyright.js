import React from "react";
import { Link } from "react-router-dom";


const Copyright = () => {
    return (
        <div className="copyright">
            <p>Copyright ©2019 Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC DCI-Indonesia. Cloud VPS Murah powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
        </div>
    )
}

export default Copyright;