import React from "react";
import "../assets/style/Content.css"


const Service = () => {
        return (
        <React.Fragment>
            <div className="service">
            <h1>Layanan Niagahoster</h1>
                <div class="card-service">
                    <div class="service-detail">
                        <img src={require("../assets/images/icon-1.svg")}></img>
                        <h4>Unlimited Hosting</h4>
                        <p>Cocok untuk website skala kecil dan menengah</p>
                        <p>Mulai dari <br/>Rp 10.000,-</p>
                    </div>
                    <div class="service-detail">
                        <img src={require("../assets/images/icons-cloud-hosting.svg")}></img>
                        <h4>Cloud Hosting</h4>
                        <p>Kapasitas resource tinggi, fully managed, dan mudah dikelola</p>
                        <p>Mulai dari <br/>Rp 150.000,-</p>
                    </div>
                    <div class="service-detail">
                        <img src={require("../assets/images/icons-cloud-vps.svg")}></img>
                        <h4>Cloud VPS</h4>
                        <p>Dedicated resource dengan akses root dan konfigurasi sendiri</p>
                        <p>Mulai dari <br/>Rp 104.000,-</p>
                    </div>
                    <div class="service-detail">
                        <img src={require("../assets/images/icons-domain.svg")}></img>
                        <h4>Domain</h4>
                        <p>Temukan nama domain yang anda inginkan</p>
                        <p>Mulai dari <br/>Rp 14.000,-</p>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <div className="service-detail-2">
                        <div className="service-detail-web">
                            <img src={require("../assets/images/icons-domain.svg")}></img>
                            <h4>Pembuatan Website</h4>
                            <p><br/>500 perusahaan lebih percayakan pembuatan<br/> websitenya pada kami. Cek selengkapnya...</p>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Service;