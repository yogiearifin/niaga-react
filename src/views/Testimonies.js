import React from "react";
import "../assets/style/Content.css"

const Testimonies = () => {
    return (
        <div class="testimonies-customer">
            <h1>Kata Pelanggan Tentang Niagahoster</h1>
            <div class="testimonies-card">
                <div class="testimonies-card-detail">
                    <img src={require("../assets/images/joseph.jpg")}></img>
                    <p>Website itu sangat penting bagi UMKM sebagai sarana promosi untuk memenangkan persaingan di era digital.</p>
                    <h4>Didik & Johan - <span>Owner Devjavu</span></h4>
                </div>
                <div class="testimonies-card-detail">
                    <img src={require("../assets/images/jonathan.jpg")}></img>
                    <p>Bagi saya Niagahoster bukan sekedar penyedia hosting melainkan partner bisnis yang bisa dipercaya.</p>
                    <h4>Bob Setyo - <span>Owner Digital Optimizer Indonesia</span></h4>
                </div>
                <div class="testimonies-card-detail">
                    <img src={require("../assets/images/jotaro.jpg")}></img>
                    <p>Solusi yang diberikan tim support Niagahoster sangat mudah dimengerti buat saya yang tidak paham teknis.</p>
                    <h4>Budi Seputro - <span>Owner Sate Ratu</span></h4>
                </div>
            </div>
        </div>
    )
}

export default Testimonies;