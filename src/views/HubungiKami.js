import React from "react";
import { Link } from "react-router-dom";


const HubungiKami = () => {
    return (
        <div className="footer">
            <ul>
                <li><h3>Hubungi Kami</h3></li>
                <li>Telp: 0274-2885822</li>
                <li>WA: 0895422447394</li>
                <li>Senin - Minggu</li>
                <li>24 Jam Non Stop</li>
                <li>Jl. Palagan Tentara Pelajar</li>
                <li>No 81 Jongkang, Sariharjo,</li>
                <li>Ngaglik, Sleman</li>
                <li>Daerah Istimewa Yogyakarta</li>
                <li>55581</li>
            </ul>
        </div>
    )
}

export default HubungiKami;