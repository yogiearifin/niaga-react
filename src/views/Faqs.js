import React from "react";
import "../assets/style/Content.css"

const Faqs = () => {
    return (
        <React.Fragment>
        <div className="faqs">
            <div class="faqs-detail">
                <h1>Pertanyaan yang Sering Diajukan</h1>
                    <div class="faqs-list">
                        <ul>
                            <li>Apa itu web hosting</li>
                            <li>Mengapa saya harus menggunakan web hosting Indonesia?</li>
                            <li>Apa yang dimaksud dengan Unlimited Hosting di Niagahoster?</li>
                            <li>Paket web hosting mana yang tepat untuk saya?</li>
                            <li>Apakah semua pembelian hosting mendapatkan domain gratis?</li>
                            <li>Jika sudah memiliki website, apakah saya bisa transfer web hosting ke Niagahoster?</li>
                            <li>Apa saja layanan web hosting Niagahoster?</li>
                        </ul>
                    </div>
            </div>
        </div>
        </React.Fragment>
    )
}

export default Faqs;