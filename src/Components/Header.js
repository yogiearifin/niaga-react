import React from "react";
import "../assets/style/Header.css"
import {Link} from "react-router-dom"


const Header = () => {
    return (
        <React.Fragment>
            <div className="header-top">
                <p>0274-2885822</p>
                <p>Live Chat</p>
            </div>
            <div className="header">
                <Link to ="/"><img src={require("../assets/images/nh-logo.png")}></img></Link>
                <Link to="unlimitedhosting"><h1>Unlimited Hosting</h1></Link>
                <Link to="cloudhosting"><h1>Cloud Hosting</h1></Link>
                <Link to="cloudvps"><h1>Cloud VPS</h1></Link>
                <Link to="domain"><h1>Domain</h1></Link>
                <Link to="afiliasi"><h1>Afiliasi</h1></Link>
                <Link to="blog"><h1>Blog</h1></Link>
                <Link to="login"><button>Login</button></Link>
            </div>
        </React.Fragment>
    )
}

export default Header;