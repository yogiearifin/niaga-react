import React from "react";
import { Link } from "react-router-dom";
import "../assets/style/Footer.css"
import HubungiKami from "../views/HubungiKami"
import Layanan from "../views/Layanan"
import ServiceHosting from "../views/ServiceHosting"
import KenapaPilih from "../views/KenapaPilih"
import Tutorial from "../views/Tutorial"
import TentangKami from "../views/TentangKami"
import Newsletter from "../views/Newsletter"
import SocialMedia from "../views/SocialMedia"
import Copyright from "../views/Copyright"
import SyaratKetentuan from "../views/SyaratKetentuan"


const FooterMain = () => {
    return (
        <React.Fragment>
            <div className="footer">
                <HubungiKami/>
                <Layanan/>
                <ServiceHosting/>
                <KenapaPilih/>
            </div>
            <div className="footer-2">
                <Tutorial/>
                <TentangKami/>
                <Newsletter/>
                <SocialMedia/>
            </div>
            <div className="footer-3">
                <Copyright/>
                <SyaratKetentuan/>
            </div>
        </React.Fragment>
    )
}

export default FooterMain;