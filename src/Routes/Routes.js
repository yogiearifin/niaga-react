import React from 'react';
import Body from "../views/Body"
import UnlimitedHosting from "../views/UnlimitedHosting"
import CloudHosting from "../views/CloudHosting"
import CloudVPS from "../views/CloudVPS"
import Domain from "../views/Domain"
import Afiliasi from "../views/Afiliasi"
import Blog from "../views/Blog"
import Login from "../views/Login"
import {Route, Router} from "react-router-dom"

function Routes() {
  return (
    <div className="content">
      <Route path="/" component={Body} exact/>
      <Route path="/unlimitedhosting" component={UnlimitedHosting} exact/>
      <Route path="/cloudhosting" component={CloudHosting} exact/>
      <Route path="/cloudvps" component={CloudVPS} exact/>
      <Route path="/domain" component={Domain} exact/>
      <Route path="/afiliasi" component={Afiliasi} exact/>
      <Route path="/blog" component={Blog} exact/>
      <Route path="/login" component={Login} exact/>
    </div>
  );
}

export default Routes;
