import React from 'react';
import Header from "./Components/Header"
import FooterMain from "./Components/FooterMain"
import Routes from "./Routes/Routes"
import "./App.css"

function App() {
  return (
    <div className="App">
      <Header/>
      <Routes/>
      <FooterMain/>
    </div>
  );
}

export default App;
